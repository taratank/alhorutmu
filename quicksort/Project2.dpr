program Project2;

{$APPTYPE CONSOLE}

uses
  SysUtils;

{ TODO -oUser -cConsole Main : Insert code here }

Const Max = 5;
Type mas = Array[1..Max] of Integer;
Const
B : mas = (10,12,9,50,1);
Var A : mas;

Procedure ShowMas;
Var k : Longint;
Begin

  For k:=1 to Max do Write(A[k]:2,' | ');
   Writeln('');
End;
 
Procedure QuickSort(left,right : Longint);
Var m,l,r : Longint;
Begin
    l:=left;
     r:=right;
     m:=((r-l) div 2)+l;
      While (l<r) do
        Begin
      While (A[l]<A[m]) and (l<m) do Inc(l);
      While (A[r]>A[m]) and (r>m) do Dec(r);
      If (A[l]>A[r]) and (l<r) then
      Begin
        A[l]:=A[l]+A[r];
        A[r]:=A[l]-A[r];
        A[l]:=A[l]-A[r];
        If l=m then m:=r else
          If r=m then m:=l;
        ShowMas;
      End;
    End;
    m:=l; {l = r}
    If left<m-1 then QuickSort(left,m-1);
    If right>m+1 then QuickSort(m,right);

End;

Begin
  A:=B;
  ShowMas;
  QuickSort(1,Max);
  readln;
End.


